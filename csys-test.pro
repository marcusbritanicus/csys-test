TEMPLATE = app
TARGET = csys-test

QT += dbus
INCLUDEPATH += .

SOURCES += csys-test.cpp storageinfo.cpp
HEADERS += storageinfo.h

CONFIG += silent

MOC_DIR       = build/moc-qt5
OBJECTS_DIR   = build/obj-qt5
RCC_DIR	      = build/qrc-qt5
UI_DIR        = build/uic-qt5
