/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 2 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QtCore>
#include <QtDebug>

// Local Headers
#include "storageinfo.h"

int main( int argc, char **argv ) {

	QCoreApplication app( argc, argv );

	StorageManager *sMgr = StorageManager::instance();
	// Q_FOREACH( StorageDevice drive, sMgr->devices() ) {
		// qDebug() << "Device         " << drive.property( "Vendor" ).toString() + " " + drive.property( "Model" ).toString();
		// qDebug() << "Id             " << drive.property( "Id" ).toString();
		// qDebug() << "Removable      " << drive.property( "Removable" ).toBool();
		// qDebug() << "Optical        " << ( drive.property( "MediaCompatibility" ).toStringList().filter( "optical" ).count() > 0 );
		// qDebug() << "Size           " << drive.property( "Size" ).toULongLong();
		// qDebug() << "Rotation Rate  " << drive.property( "RotationRate" ).toInt();
		// qDebug() << "Seat           " << drive.property( "Seat" ).toString();
		// qDebug() << "";

		// Q_FOREACH( StorageBlock block, drive.partitions() ) {
			// qDebug() << "    Label     " << block.label();
			// qDebug() << "    Path      " << block.path();
			// qDebug() << "    Dev       " << block.device();
			// qDebug() << "    Drive     " << block.drive();
			// qDebug() << "    Mount     " << block.mountPoint();
			// qDebug() << "    FS        " << block.fileSystem();
			// qDebug() << "    Size      " << block.totalSize();
			// qDebug() << "    Optical   " << block.isOptical();
			// qDebug() << "    Removable " << block.isRemovable();
			// qDebug() << "    PartNo    " << block.property( "Partition", "Number" ).toInt();
			// qDebug() << "    Container " << block.property( "Partition", "IsContainer" ).toBool();
			// qDebug() << "-------------------------------------------------------------------------------------------------------";
		// }

		// qDebug() << "=======================================================================================================";
		// qDebug() << "";
	// }

	return 0;
};
